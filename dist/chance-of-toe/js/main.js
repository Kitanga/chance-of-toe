var game = new Phaser.Game({
    title: gameData.title,
    version: gameData.version,
    url: gameData.url,

    width: gameData.width,
    height: gameData.height,
    resolution: window.devicePixelRatio,
    type: Phaser.AUTO,
    scene: [
        // Match,
        Boot,
        Preload,
        MainMenu,
        Tutorial,
        Play,
        PostMatch
    ],
    input: {
        keyboard: false
    }
});