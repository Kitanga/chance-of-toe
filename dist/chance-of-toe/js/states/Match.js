/// <reference types="firebase" />
// Initialize Firebase
var config = {
    apiKey: "AIzaSyB2L4UB5eCuoET_Kg2fxPu79tnSYKqE7wQ",
    authDomain: "chance-of-toe.firebaseapp.com",
    databaseURL: "https://chance-of-toe.firebaseio.com",
    projectId: "chance-of-toe",
    storageBucket: "chance-of-toe.appspot.com",
    messagingSenderId: "217311794887"
};
firebase.initializeApp(config);
var db = firebase.database().ref('/matches');

// The username we'll send to the other player
var username = "";
// The user's uid
var uid = uuidv1();

function findMatch() {
    return checkForMatch(username)
        .then(startMatch)
        .then(makeMatch)
        .catch(function (err) {
            console.error(err);
        });
}

/**
 * Find a free match for the client to join, if there's none, then make one and wait for an opponent to join
 * 
 * @param {string} username The username of the player
 */
function checkForMatch() {
    // Get the first item with an empty or none existent 'p2' child key
    console.log("Searching for an open match");
    return db.orderByChild('p2').limitToFirst(1).once('value');
}

/**
 * Run when we find a match in the realtime database
 */
function startMatch(snap) {
    // Use the snapshot returned to determine whether we start a new match or not
    // console.log("Response:", snap);
    var matchQuery = snap.val();

    for (var ix in matchQuery) {
        if (matchQuery.hasOwnProperty(ix)) {
            var matchResult = matchQuery[ix];
            console.log("Match query result:", matchResult);

            // If the 'p2' child key doesn't exist, that means that this is a free match. Now run a transaction and return false
            if (!matchResult.p2) {
                // Transaction
                console.log('Found Match!!!!');
                return false;
            } /* else {
                console.log("Found nothing. Trying to create a match");
                return true;
            } */
        }
    }
    console.log("Found nothing. Trying to create a match");
    return true;
}

/**
 * Run when we can't find a free match in database.
 */
function makeMatch(shouldMakeMatch) {
    // console.log("In the make match function");
    // If true, we ask the server to make a new match for us
    if (shouldMakeMatch) {
        // 
        console.log("Started creating a match");
        var createMatch = firebase.functions().httpsCallable('createMatch');
        createMatch({
            uid: uid,
            name: username
        }).then(function (result) {
            console.log("Match created!! Here's the data from server", result);
            // Now we listen for any changes to the object
            return db.child(result.data).on('child_added', waitForOpponent)
        });
    } else {
        return
    }
}

function waitForOpponent(snap) {
    var data = snap.val();
    if (data.uid !== uid) {
        console.log("Waiting for opponent:", data);
        var opponent = {};
        opponent.username = data.name;
        return opponent;
    }
}


var Match = new Phaser.Scene('Match');
Match.create = function () {
    // 
    username = prompt('Your Username:', 'Human') || 'Human';
    findMatch().then(function(opponent) {
        console.log("Match has begun! Opponent:", opponent);
    });
};