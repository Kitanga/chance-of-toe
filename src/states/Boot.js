var gameData = {
    title: "Chance of Toe",
    version: "0.1.0",
    url: "https://www.kongregate.com/games/kng_vulture/chance-of-toe",
    width: window.innerWidth,
    height: window.innerHeight
};

var Boot = new Phaser.Scene('Boot');

Boot.init = function (data) {};
Boot.preload = function () {};
Boot.create = function (data) {
    this.scene.start('Preload');
};
Boot.update = function (time, delta) {};