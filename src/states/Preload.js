var Preload = new Phaser.Scene('Preload');

Preload.init = function (data){};
Preload.preload = function (){
    this.load.baseURL = location.href;
    this.load.audio('select', ['/snd/sfx/select.m4a']);
    this.load.audio('win1', ['/snd/sfx/double_score.ogg']);
    this.load.audio('win2', ['/snd/sfx/triple_score.ogg']);

    this.load.bitmapFont('m5x7', 'bitmap_font/m5x7_0.png', 'bitmap_font/m5x7.xml');
};
Preload.create = function (data){
    // var select = this.sys.sound.add('select').on('');
    this.scene.start('MainMenu');
};
Preload.update = function (time, delta){};