var Tutorial = new Phaser.Scene('Tutorial');

Tutorial.init = function (data) {};
Tutorial.preload = function () {};
Tutorial.create = function (data) {
    // this.scene.start('Play');
    // return;
    // Ref to this scene
    var _this = this;
    // Show the tutorial div
    var tutorial = document.getElementById('tutorial');
    tutorial.style = "display: flex;";

    // Set the onclick event for the tutorial div to be hidden
    tutorial.onclick = function () {
        this.style = 'display: none;';
        _this.scene.start('Play');
        this.onclick = null;
        this.ontouchstart = null;
    };
    tutorial.ontouchstart = tutorial.onclick;
};
Tutorial.update = function (time, delta) {};