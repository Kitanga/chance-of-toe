var Play = new Phaser.Scene('Play');

var username = !!localStorage.getItem('name') ? localStorage.getItem('name') : prompt("Your username:", "You");
localStorage.setItem('name', username);

Play.init = function (data) {};
Play.preload = function () {};
Play.create = function (data) {
    // Ref to this scene
    var _this = this;

    // Load audio
    var addStoneSfx = this.sound.add('select');
    var score1Sfx = this.sound.add('win1'
        /* , {
                volume: .52
            } */
    );
    var score2Sfx = this.sound.add('win2'
        /* , {
                volume: .52
            } */
    );

    // Ref to to main camera
    var camera = this.cameras.main;
    camera.setBackgroundColor('#0d0420');
    // Center of the screen
    var x = Math.floor(gameData.width / 2);
    var y = Math.floor(gameData.height / 2);

    // Grid dimensions
    var ratio = .61;
    var height = Math.round(gameData.height * ratio);
    var width = height;

    // The stone about to be placed, 0 is nought and 1 is cross
    var currentStone = Phaser.Math.Between(0, 1);

    // Grid position
    var gridX = x - Math.floor(width / 2);
    var gridY = y - Math.floor(width / 2);

    // Grid cell padding
    var padding = {
        right: 2,
        top: 2,
        left: 2,
        bottom: 2,
    };

    // Grid cell dimensions
    var unpaddedGridW = Math.floor((width / 7));
    var unpaddedGridH = Math.floor((height / 7));
    var gridW = unpaddedGridW - (padding.left + padding.right);
    var gridH = unpaddedGridH - (padding.top + padding.bottom);
    var halfGridW = Math.floor(gridW / 2);
    var halfGridH = Math.floor(gridH / 2);

    //#region Create all the textures for the cells/tile, nought, and cross sprites.
    // Create the graphic used by an empty cell
    var cell = this.add.graphics();
    cell.setActive(false).setVisible(false);
    cell.lineStyle(7, 0x3e317c);
    cell.strokeRect(0, 0, gridW, gridH);
    cell.generateTexture('cell', gridW, gridH);

    // Noughts graphic
    var oGpc = this.add.graphics();
    var radius = Math.floor(halfGridW * .47);
    oGpc.setActive(false).setVisible(false);
    oGpc.lineStyle(9, 0x3e317c);
    oGpc.beginPath();
    oGpc.arc(halfGridW, halfGridH, radius, 0, Math.PI * 2);
    oGpc.strokePath();
    oGpc.closePath();
    oGpc.lineStyle(7, 0x3e317c);
    oGpc.strokeRect(0, 0, gridW, gridH);
    oGpc.generateTexture('nought', gridW, gridH);

    // Crosses graphic
    var xGpc = this.add.graphics();
    var pad = halfGridW - radius;
    xGpc.setActive(false).setVisible(false);
    xGpc.lineStyle(9, 0x3e317c);
    xGpc.beginPath();

    xGpc.moveTo(pad, pad);
    xGpc.lineTo(gridW - pad, gridH - pad);
    xGpc.moveTo(pad, gridH - pad);
    xGpc.lineTo(gridW - pad, pad);

    xGpc.moveTo(0, 0);

    xGpc.strokePath();
    xGpc.closePath();

    // Create the border
    xGpc.lineStyle(7, 0x3e317c);
    xGpc.strokeRect(0, 0, gridW, gridH);

    xGpc.generateTexture('cross', gridW, gridH);
    //#endregion

    // The player and opponent score, and timer
    this.playerScore = 0;
    this.opponentScore = 0;
    this.timeLeft = 43 * 1000;

    // Text for Player score
    var __you = this.add.bitmapText(0, 0, 'm5x7', username, 25, 1);
    __you.setPosition(gridX + padding.left, gridY - (__you.height * 2));
    this.playerScoreText = this.add.bitmapText(0, 0, 'm5x7', this.playerScore, 25, 1);
    this.playerScoreText.setPosition(gridX + padding.left, gridY - (__you.height));

    // Text for opponent score
    var __opponent = this.add.bitmapText(0, 0, 'm5x7', 'Opponent', 25, 2);
    __opponent.setPosition(gridX - padding.left + width - __opponent.width - padding.right, gridY - (__opponent.height * 2));
    this.opponentScoreText = this.add.bitmapText(0, 0, 'm5x7', this.opponentScore, 25, 2);
    this.opponentScoreText.setPosition(gridX - padding.left + width - this.opponentScoreText.width - padding.right, gridY - (__opponent.height));

    // Timer
    var __timer = this.add.bitmapText(0, 0, 'm5x7', 'Time Left', 25, 1);
    __timer.setPosition(x, gridY - (__timer.height * 2)).setOrigin(0.5);
    this.timerText = this.add.bitmapText(0, 0, 'm5x7', Math.round((this.timeLeft / 1000)) + 's', 25, 1).setOrigin(0.5);
    this.timerText.setPosition(x, gridY - (__timer.height));

    // Noughts and Crosses key/value map for placed stones
    var noughts = this.add.group();
    var crosses = this.add.group();

    var noughtColor = Phaser.Display.Color.HexStringToColor('#fc3c3c').color;
    var crossColor = Phaser.Display.Color.HexStringToColor('#1874c3').color;

    // The stone player will place
    var nextStoneX = Math.floor(gameData.width / 2);
    var nextStoneY = Math.floor(gameData.height * (1 - (ratio / 4)));
    var oStone = this.add.sprite(nextStoneX, nextStoneY, 'nought');
    oStone.setTintFill(noughtColor);
    var xStone = this.add.sprite(nextStoneX, nextStoneY, 'cross');
    xStone.setTintFill(crossColor);

    // Set the visibility depending on which stone is next
    this.currentStoneShow(currentStone, oStone, xStone);

    // Create the grid
    var grid = this.add.group();

    // The neighbours array
    var neighbours = [];
    for (var ix = 0; ix < 7; ix++) {
        neighbours[ix] = [];
    }

    // Opponent AI variables
    var aiActive = true;
    var prevStone = 0;
    var prevSelectedSprite = null;
    var totalTiles = 0;
    var aiPlayed = true;

    // Fill the grid with cells
    for (var ix = 0; ix < 7; ix++) {
        var x = gridX + padding.left + (unpaddedGridW * ix);
        for (var kx = 0; kx < 7; kx++) {
            var y = gridY + padding.top + (unpaddedGridH * kx);

            // Create the sprite that will represent a cell
            var sprite = this.add.sprite(x, y, 'cell');
            sprite.setOrigin(0);
            sprite.hasBeenCollected = false;
            sprite.neighbours = [];
            neighbours[kx][ix] = sprite;

            // Add input
            sprite.setInteractive({
                useHandCursor: true
            });
            // When the cell is clicked on
            sprite.on('pointerup', function (aiShouldPlay, aiTurn) {
                // console.log('Click!');

                if (!this.stonePlaced && (aiPlayed || aiTurn)) {
                    aiPlayed = false;
                    // Place the stone
                    this.setVisible(false);
                    this.stonePlaced = true;
                    if (currentStone) {
                        this.cross.setVisible(true);
                        this.isNought = false;
                        this.setTintFill(crossColor);
                        // oGrid[this.gridPos.x + ',' + this.gridPos.y] = true;
                    } else {
                        this.nought.setVisible(true);
                        this.isNought = true;
                        this.setTintFill(noughtColor);
                    }
                    _this[aiShouldPlay ? 'playerScore' : 'opponentScore'] += _this.hasScored(this, function (collected) {
                        try {
                            if (collected.length < 2) {
                                addStoneSfx.play();
                            } else {
                                score1Sfx.play();
                            }
                        } catch (error) {
                            console.error(error);
                        }
                        camera.shake(70, 0.007);
                    }, function () {
                        try {
                            score2Sfx.play();
                        } catch (error) {
                            console.error(error);
                        }
                        camera.shake(140, 0.0092);
                    });

                    // Update the player/opponent score depending on who's just clicked
                    _this[aiShouldPlay ? 'playerScoreText' : 'opponentScoreText'].setText(_this[aiShouldPlay ? 'playerScore' : 'opponentScore']);

                    grid.remove(this);
                    prevStone = currentStone;

                    // Hide the tile that shows which stone is next.
                    // Alternatively, show an ellipsis "..."
                    xStone.setVisible(false);
                    oStone.setVisible(false);

                    currentStone = Phaser.Math.Between(0, 1);

                    // Now AI places a stone
                    var _this_sprite = this;
                    // console.log("The sprite has been selected by:", aiShouldPlay ? 'player!' : 'AI!');
                    setTimeout(function () {
                        // TODO: This AI is too easy. Create a more complex and difficult opponent by looking for an element to pick. And change whether it searches from the bottom up or top down to keep the player guessing.
                        if (aiActive && --totalTiles && aiShouldPlay) {
                            if (prevStone === currentStone) {
                                _this.getFreeNeighbour(_this_sprite.neighbours, grid.getChildren()).emit('pointerup', false, true);
                            } else {
                                _this.getRandomFreeTile(grid.getChildren()).emit('pointerup', false, true);
                            }
                        }
                        _this.currentStoneShow(currentStone, oStone, xStone);
                        aiPlayed = true;

                        // Make sure all the stones on the grid have at least one1 neighbour
                        // _this.has
                    }, 520);
                }
            }, sprite);

            // The cell's on hover event
            sprite.on('pointermove', function () {
                if (!this.stonePlaced && aiPlayed) {
                    if (currentStone) {
                        this.setTintFill(crossColor);
                    } else {
                        this.setTintFill(noughtColor);
                    }
                }
            }, sprite);

            // When the mouse moves off the cell
            sprite.on('pointerout', function () {
                this.clearTint();

                // if (this.stonePlaced && this.isNought) {
                //     this.setTintFill(noughtColor);
                // } else if (this.stonePlaced && !this.isNought) {
                //     this.setTintFill(crossColor);
                // }
            }, sprite);

            grid.add(sprite);

            // Now add the same one into the nought and cross group as well
            var oSpr = this.add.sprite(x, y, 'nought');
            var xSpr = this.add.sprite(x, y, 'cross');

            // Hide the tile
            oSpr.setVisible(false);
            xSpr.setVisible(false);

            // The stone's color
            oSpr.tintFill = true;
            xSpr.tintFill = true;
            oSpr.setTintFill(noughtColor);
            xSpr.setTintFill(crossColor);

            // Set the tile origin to it's top left most position
            oSpr.setOrigin(0);
            xSpr.setOrigin(0);

            // Get the reference for the two different stones (nought and cross) above our sprite/cell/tile
            sprite.nought = oSpr;
            sprite.cross = xSpr;

            // Flag for determining whether we have placed the stone onto the tile
            sprite.stonePlaced = false;
        }
    }
    totalTiles = grid.children.size;

    for (var ix = 0; ix < neighbours.length; ix++) {
        for (var kx = 0; kx < neighbours[ix].length; kx++) {
            var neighbour = neighbours[ix][kx];
            for (var nx = -1; nx < 2; nx++) {
                for (var jx = -1; jx < 2; jx++) {
                    // Make sure the element exists
                    if (!!neighbours[ix + jx] && !!neighbours[ix + jx][kx + nx] &&
                        ((!nx && jx) ||
                            (nx && !jx))) {
                        // Add as a neighbour
                        neighbour.neighbours.push(neighbours[ix + jx][kx + nx]);
                        // neighbours[kx + jx][ix + nx].neighbours.push(sprite);
                    }
                }
            }
        }
    }

    this.startMatch()
    // console.log(neighbours[0][0].neighbours);
    // console.log(neighbours[0][1].neighbours);
    // console.log(neighbours[1][1].neighbours);
    // console.log(neighbours[1][1].eventNames());
};

Play.startMatch = function () {
    // 
    var _this = this;
    this.__timer = setInterval(function () {
        if (_this.timeLeft) {
            _this.timeLeft -= 1000;
            _this.timerText.setText(Math.round((_this.timeLeft / 1000)) + 's');
        } else {
            _this.timerText.setText('0');
            clearInterval(_this.__timer);
            alert('Game is Done!!!!!!!\n' + (_this.playerScore > _this.opponentScore ? 'You win!' : (_this.playerScore === _this.opponentScore ? "It's a draw." : "You lose.")));
            _this.endMatch();
        }
    }, 1000);
};

Play.endMatch = function () {
    this.scene.start('Play');
};

Play.currentStoneShow = function (currentStone, oStone, xStone) {
    // If the next stone will be nought/cross, make show it
    if (currentStone) {
        xStone.setVisible(true);
        oStone.setVisible(false);
    } else {
        oStone.setVisible(true);
        xStone.setVisible(false);
    }
};

Play.hasScored = function (sprite, failureCallback, successCallback) {
    // Our returned score
    var score = 0;
    var collected = [sprite];

    // Add the score plus one for the current sprite
    score++;

    // Check the current neighbours list for any that might share the same isNought value
    for (var ix = 0, length = sprite.neighbours.length; ix < length; ix++) {
        if (sprite.neighbours[ix].stonePlaced &&
            sprite.neighbours[ix].isNought === sprite.isNought &&
            !sprite.neighbours[ix].hasBeenCollected) {

            collected.push(sprite.neighbours[ix]);
            if (++score < 3) {
                continue;
            } else {
                // console.log('Break! IX:', ix);
                break;
            }
        }
    }
    // console.log('First:', score, collected.length);

    // Now if the score is less than 3 then we go into the neighbours neighbours, except for us of course
    if (score < 3) {
        // 
        for (var nx = 0, length = sprite.neighbours.length; nx < length; nx++) {
            var shouldContinue = true;
            var neighbour = sprite.neighbours[nx];

            // If the neighbour has not been picked and has the same stone type as what has been placed, then continue on with what you are doing
            if (!neighbour.hasBeenCollected && neighbour.isNought === sprite.isNought) {
                // Checking the neighbours neighbours
                for (var ix = 0, length2 = neighbour.neighbours.length; ix < length2; ix++) {
                    // Make sure we don't check the main neighbour
                    if (neighbour.neighbours[ix] !== sprite &&
                        neighbour.neighbours[ix].stonePlaced &&
                        !neighbour.neighbours[ix].hasBeenCollected &&
                        neighbour.neighbours[ix].isNought === sprite.isNought) {
                        collected.push(neighbour.neighbours[ix]);
                        if (++score < 3) {
                            continue;
                        } else {
                            shouldContinue = false;
                            break;
                        }
                    }
                }
            }

            if (!shouldContinue) {
                break;
            }
        }
    }
    // console.log("Second Score:", score, collected.length);

    // Here we check if the score is less than 3 again. If it is, there aren't enough stones around for the player to win
    if (score < 3) {
        !!failureCallback ? failureCallback(collected) : '';
    } else {

        for (var ix = 0, length = collected.length; ix < length; ix++) {
            collected[ix].hasBeenCollected = true;
            if (collected[ix].isNought) {
                collected[ix].nought.clearTint();
                collected[ix].nought.setTintFill(0x3e317c);
            } else {
                collected[ix].cross.clearTint();
                collected[ix].cross.setTintFill(0x3e317c);
            }
            // console.log(collected[ix]);
        }!!successCallback ? successCallback(collected) : '';
    }

    // console.log('Score:', score);
    if (score === 3) {
        return 5;
    } else if (score === 2) {
        return 3;
    } else {
        return 0;
    }
};
Play.getFreeNeighbour = function (arr, gridChildren) {
    var spr = null;
    var foundValidSprite = false;
    for (var ix = 0, {
            length
        } = arr; ix < length; ix++) {
        if (!arr[ix].stonePlaced) {
            spr = arr[ix];
            foundValidSprite = true;
            break;
        }
    }

    if (foundValidSprite) {
        return spr;
    } else {
        return this.getRandomFreeTile(gridChildren);
    }
};
Play.getRandomFreeTile = function (arr) {
    var spr = Phaser.Utils.Array.GetRandom(arr);

    if (spr.stonePlaced) {
        spr = this.getFreeNeighbour(arr);
    }

    /** @type Phaser.Sprite */
    return spr;
};

Play.update = function (time, delta) {};