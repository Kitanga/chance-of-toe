var MainMenu = new Phaser.Scene('MainMenu');

MainMenu.init = function (data) {};
MainMenu.preload = function () {};
MainMenu.create = function (data) {
    // this.scene.start('Tutorial');
    // return;
    // Get a reference to the current scene
    var _this = this;

    // Create an event listener that checks when the screen is touched/pressed on
    window.onclick = function () {
        _this.scene.start('Tutorial');
        this.onclick = null;
        this.ontouchstart = null;
    };

    window.ontouchstart = window.onclick;
    // Position at which we'll add the text onto the screen
    var x = this.sys.cameras.cameras[0].centerX;
    var y = this.sys.cameras.cameras[0].centerY;

    // Message we'll show on screen
    var message = (this.game.device.input.touch ? 'Touch' : 'Click on screen') + ' to start game';

    // Add text to the scene
    var text = this.add.text(x, y, message, {
        style: {
            align: 'center'
        }
    });

    // Set the origin to the center
    text.setOrigin(0.5);
};
MainMenu.update = function (time, delta) {};