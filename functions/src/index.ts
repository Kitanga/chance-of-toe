import * as functions from 'firebase-functions';

import * as admin from 'firebase-admin';
admin.initializeApp();

interface Data {
    uid: string;
    name: string;
}

// The realtime database and ref to the matches object
const db = admin.database();
const matches = db.ref('/matches');

// Create a new match and return the key to it
exports.createMatch = functions.https.onCall((data: Data) => {
    return matches.push({
            p1: {
                uid: data.uid,
                username: data.name
            }
        }).key;
});