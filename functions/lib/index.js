"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const functions = require("firebase-functions");
const admin = require("firebase-admin");
admin.initializeApp();
// The realtime database and ref to the matches object
const db = admin.database();
const matches = db.ref('/matches');
// Create a new match and return the key to it
exports.createMatch = functions.https.onCall((data) => {
    return matches.push({
        p1: {
            uid: data.uid,
            username: data.name
        }
    }).key;
});
//# sourceMappingURL=index.js.map